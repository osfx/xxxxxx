class TelegramWebhooksController < Telegram::Bot::UpdatesController
  def start(*)
    respond_with :message, text: t('.hi')
  end

  def add(*)
    respond_with :message, text: t('.qe')
  end
end
